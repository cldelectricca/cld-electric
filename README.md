San Diego Electrician committed to the highest standards of quality electrical troubleshooting, repair and service. We can handle everything from small household repairs to large scale whole home or office rewiring projects. Ask about our Senior Discount.

Address: 5519 Clairemont Mesa Blvd, San Diego, CA 92117, USA

Phone: 619-638-0228

Website: http://www.cldelectric.com
